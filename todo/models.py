from django.db import models

# Create your models here.
class Todo(models.Model):
    title = models.CharField(max_length=120)
    description = models.TextField()
    completed = models.BooleanField(default=False)

    def _str_(self):
        return self.title

class User(models.Model):
    name = models.CharField(max_length=120)
    email = models.CharField(max_length=120)

    def _str_(self):
        return self.name

class Mail(models.Model):
    content = models.TextField()
    subject = models.TextField()
    sender = models.ForeignKey(User, on_delete=models.CASCADE,related_name='user_sender')
    receiver = models.ForeignKey(User, on_delete=models.CASCADE,related_name='user_receiver')

    def _str_(self):
        return self.content
