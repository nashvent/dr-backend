from django.shortcuts import render
from rest_framework import generics
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.decorators import api_view

from rest_framework import viewsets          
from .serializers import TodoSerializer      
from .models import Todo                     

from .serializers import MailSerializer      
from .models import Mail                     

from .serializers import UserSerializer      
from .models import User                     
from django.db.models import Q
from django.http import JsonResponse

class TodoView(viewsets.ModelViewSet):       
    serializer_class = TodoSerializer          
    queryset = Todo.objects.all()        

class UserView(viewsets.ModelViewSet):       
    serializer_class = UserSerializer          
    queryset = User.objects.all()

class MailView(viewsets.ModelViewSet):       
    serializer_class = MailSerializer          
    queryset = Mail.objects.all()




@api_view()
def messageReceive(request,user):
    queryset = Mail.objects.prefetch_related('sender').filter(receiver=user)    
    #for quer in range(len(queryset)):
    #    print(quer)
    #return JsonResponse(queryset.values())
    return Response({"mails":queryset.values()})
    
@api_view()
def messageSend(request,user):
    queryset = Mail.objects.prefetch_related('receiver').filter(sender=user)    
    return Response({"mails":queryset.values()})

@api_view(['POST'])
def messageStore(request,user):
    print(request,"pasnadooo")
    #queryset = Mail.objects.prefetch_related('receiver').filter(sender=user)    
    return Response({})
    